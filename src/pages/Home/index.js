import React from 'react'
import { ListOfCategories } from '../../components/ListOfCategories'
import { ListOfPhotoCards } from '../../containers/ListOfPhotoCards'
import { withRouter } from'react-router-dom'
// import { Helmet } from 'react-helmet'
import { Layout } from '../../components/Layout'


const Home = (props) => {
  const { id } = props.match.params
  // console.log(parseFloat(id))
  return (
    <Layout title='Home'  verDatos = {false} subtitle ='Con Petgram puedes encontrar fotos de animales domésticos muy fácilmente, jvicuna' > 
      {/* <Helmet>
        <title>Home | Petgram 🐶</title>
        <meta name='description' content='Con Petgram puedes encontrar fotos de animales domésticos muy fácilmente' />
      </Helmet> */}
      <ListOfCategories />
      <ListOfPhotoCards categoryId ={parseFloat(id)} />
    </Layout>
  )
}
export default withRouter(Home)


import React from 'react'
import { useAuthContext } from '../../context/AppContext'
import { UserForm } from '../../components/UserForm'
import { useRegisterMutation } from '../../containers/RegisterMutation'
// import { useLoginMutation } from '../../containers/LoginMutation'
import { Layout } from '../../components/Layout'

import { Link } from 'react-router-dom';

export const RegisterPage = () => {
  const { toggleAuth } = useAuthContext()
  const { registerMutation,dataRegister,loadingRegister,errorRegister } = useRegisterMutation();
  // const { loginMutation,dataLogin,loadingLogin,errorLogin } = useLoginMutation();

  const onSubmitRegister = ({email, password}) => {
    const input = { email, password }
    const variable = { input }
    registerMutation({variables: variable})
    .then(       response =>  {
      toggleAuth(response.data.signup)
      
    })
    .catch( (err) => {console.log('No se puede registrar el usuario. Ya existe o los datos no son correctos.')});
} 
  const errorMsg = errorRegister && 'No se puede registrar el usuario. Ya existe o los datos no son correctos.'
  return (

    <Layout title='Autentificación' subtitle='Regístrate en Petgram para poder acceder a esta sección'>      
      <UserForm disabled = {loadingRegister} error = {errorMsg} title='Regístrate' onSubmit={onSubmitRegister} />
      <div style={{display: 'flex', justifyContent: 'center'}}>
        <span style={{marginRight:'8px'}}>¿Ya tienes una cuenta?  </span>
        <Link to={`/login`} style={{textDecoration: 'none'}}>
          <span style={{color:'#0094e1', fontWeight:'600'}}>Inicia sesión</span>
        </Link>
      </div>
    </Layout>

  )
}
import React, {useContext } from 'react'
import {useAuthContext} from '../../context/AppContext'
import { Button } from '../../components/Button/index'
import { Layout } from '../../components/Layout'

export const Profile = () => {
  const { removeAuth } = useAuthContext()
  return  <>
    <Layout title='Tu perfil'>
      <Button onClick={removeAuth}>Cerrar sesión</Button>
    </Layout>
  </>

}
import React from 'react'
import {RenderPropFavs} from '../../containers/GetFavorites'
import { Layout } from '../../components/Layout'

export const Favs = () => {
  return (
      <>
      <Layout title='Tus favoritos' subtitle='Aquí tienes las fotos que te han gustado'>
        <RenderPropFavs/>
      </Layout>
    </>

  )
}
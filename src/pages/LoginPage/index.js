import React from 'react'
import { useAuthContext } from '../../context/AppContext'
import { UserForm } from '../../components/UserForm'
import { useLoginMutation } from '../../containers/LoginMutation'
import { Layout } from '../../components/Layout'

import { Link } from 'react-router-dom';

export const LoginPage = () => {
  const { toggleAuth } = useAuthContext()
  const { loginMutation,dataLogin,loadingLogin,errorLogin } = useLoginMutation();


  const onSubmitLogin = ({email, password}) => {
    const input = { email, password }
    const variable = { input }
    loginMutation({variables: variable})
    .then(   response =>  {
      toggleAuth(response.data.login)
    })
    .catch( (err) => {console.log('No se puede iniciar sesión. El usuario no existe o el password no es correcto.')});
  } 

  const errorLg = errorLogin && 'No se puede iniciar sesión. El usuario no existe o el password no es correcto.'
  return (

    <Layout title='Autentificación' subtitle='Inicia sesión en Petgram para poder acceder a esta sección'>      
      <UserForm disabled = {loadingLogin} error = {errorLg} title='Iniciar Sesion' onSubmit={onSubmitLogin} />
      <div style={{display: 'flex', justifyContent: 'center'}}>
        <span style={{marginRight:'8px'}}>¿No tienes cuenta?  </span>
        <Link to={`/register`} style={{textDecoration: 'none'}}>
          <span style={{color:'#0094e1',fontWeight:'600'}}>Registrate</span>
        </Link>
      </div>
    </Layout>

  )
}
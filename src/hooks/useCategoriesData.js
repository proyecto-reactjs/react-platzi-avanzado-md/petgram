import { useEffect, useState,useRef } from 'react'

export const useCategoriesData = () => {
  const [categories, setCategoriesData] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const isMounted = useRef(true);

  useEffect( () => {
    return () => {
        isMounted.current = false;
    }
  }, [])

  useEffect(function () {
    setLoading(true)
      window.fetch('https://petgram-serve-jorge-vicuna.vercel.app/categories')
        .then(res => res.json())
        .then(categories => {
          if ( isMounted.current ) {
            setCategoriesData(categories)
          }
        })
        .catch(err => {
          setError(err)
        })
        .finally(() => {
          setLoading(false)
        })
  }, [])

  return { error, loading, categories }
}


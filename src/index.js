import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {ApolloClient,InMemoryCache,createHttpLink,ApolloProvider} from "@apollo/client"
import { setContext } from "@apollo/client/link/context";
import { onError } from '@apollo/client/link/error'

    const httpLink  = createHttpLink ({
        uri: 'https://petgram-serve-jorge-vicuna.vercel.app/graphql'
    });
    
    const authLink = setContext ((_, { headers }) => { // get the authentication token from local storage if it exists
        const token = JSON.parse(window.localStorage.getItem('token')) // return the headers to the context so httpLink can read them
        return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : "",
        }
        }
    });

    const client = new ApolloClient({

        link: authLink.concat(httpLink),
        cache: new InMemoryCache(),
      
        onError: onError(
          ({ networkError }) => {
            if (networkError && networkError.result.code === 'invalid_token') {
              window.localStorage.removeItem('token')
              window.location.href = '/'
            }
          } 
        )
      
      })
      

    ReactDOM.render(

            <ApolloProvider client={client}>
                <App />
            </ApolloProvider>,

        document.getElementById("app")
    );
import React,{useState} from 'react'
// import { useInputValue } from '../../hooks/useInputValue'
import { Form, Input, Button, Title,Error,Espacio } from './styles'
// import LoadingIcons from 'react-loading-icons'

import { css } from "@emotion/react";
import RingLoader from "react-spinners/RingLoader";


import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField'
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color:  "#fff";
`;

export const UserForm = ({ disabled, error, title, onSubmit }) => {
  // const email = useInputValue('')
  // const password = useInputValue('')

  const [form, setForm] = useState({email: '',password: ''});
  const handleForm = e => {
    setForm({
        ...form,
        [e.target.name]: e.target.value,
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit({ email: form.email, password: form.password });

  }
  // const handleSubmit = e => {
  //   e.preventDefault()
  //   onSubmit({ email: email.value, password: password.value })
  // }
  const linked = 'https://i.pinimg.com/originals/a2/dc/96/a2dc9668f2cf170fe3efeb263128b0e7.gif'
  return (
    <>
      <Form disabled= {disabled}onSubmit={handleSubmit} > 

        <Title>{title}</Title>

        <Box
          sx={{
            '& > :not(style)': { my: 1, width: '100%' },
              }}
              // autoComplete="off"
        >
          <TextField id="outlined-basic" label="Email" variant="outlined" name="email" type="email" value={form.email} fullWidth  onChange={handleForm} />
          <TextField id="outlined-basic" label="Password" variant="outlined" name="password" fullWidth  type="password" value={form.password} onChange={handleForm} />
        </Box>

          {/* <Input placeholder='Email' {...email} />
        <Input placeholder='Password' type='password' {...password} /> */}
        {/* <Input disabled= {disabled} placeholder="Email" name="email" value={form.email} onChange={handleForm} />
        <Input disabled= {disabled} placeholder="Password" name="password" type="password" value={form.password} onChange={handleForm} /> */}
       
        <Button disabled= {disabled} > 
          {disabled ? 
            <RingLoader color = {"#fff"}loading={disabled} css={override} size={30} />
          // <LoadingIcons.TailSpin stroke="#fff" strokeOpacity={1} speed={.75} height= {30}/>
          :  <>{title}</>
           }
        </Button>


      </Form>

      {error ? 
      <>
      <Espacio>
        <Stack sx={{ width: '100%', border: '1px solid red'}} spacing={2}>
          <Alert severity="error">{error}</Alert>
        </Stack>
      </Espacio>
      </>
      // <Error>{error}</Error> 
      : <Espacio></Espacio>}
    </>
  )
}

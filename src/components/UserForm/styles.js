import styled from 'styled-components'

export const Error = styled.small`
  font-size: 10px;
  color: red;
  height:11vh;
  width: 100%;
  display: block;
`
export const Espacio = styled.div`
  height:11vh;
  width: 100%;
`
export const Title = styled.h2`
  font-size: 16px;
  font-weight: 500;
  padding: 8px 0;
`

export const Form = styled.form`
  padding: 16px 0;
  &[disabled] {
    opacity: .3;
    pointer-events: none;
  }
`

export const Input = styled.input`
  border: 1px solid #ccc;
  border-radius: 3px;
  margin-bottom: 8px;
  padding: 8px 4px;
  display: block;
  width: 100%;
`

export const Button = styled.button`
  margin-top:4vh ;
  color: #fff;
  height: 38px;
  display: block;
  width: 100%;
  text-align: center;
  font-size: 15px;
  -webkit-box-shadow: 0px 1px 3px #666666;
  -moz-box-shadow: 0px 1px 3px #666666;
  box-shadow: 0px 1px 3px #666666;
  text-shadow: 1px 1px 3px #666666;
  background: -webkit-gradient(linear, 0 0, 0 100%, from(#00e1ff), to(#0094e1));
  background: -moz-linear-gradient(top, #00e1ff, #0094e1);

  /* background: #00c4f3fc;
  border-radius: 3px;
  color: #fff;
  height: 32px;
  display: block;
  width: 100%;
  text-align: center;
  font-size: 15px */
`
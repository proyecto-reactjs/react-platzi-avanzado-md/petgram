import React from 'react'
import { Button } from './styles'

import { MdFavorite, MdFavoriteBorder } from 'react-icons/md'
import PropTypes from 'prop-types';

export const FavButton = ({ like, likes, onClick }) => {
  const Icon = like? MdFavorite : MdFavoriteBorder

  return (
    <Button onClick={onClick}>
      <Icon size='24px' /> {likes} likes!
    </Button>
  )
}

FavButton.propTypes = {
  like :PropTypes.bool.isRequired,
  likes :PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
}
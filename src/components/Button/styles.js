import styled from 'styled-components'

export const StyledButton = styled.button`
  color: #fff;
  height: 38px;
  display: block;
  width: 100%;
  text-align: center;
  font-size: 15px;
  -webkit-box-shadow: 0px 1px 3px #666666;
  -moz-box-shadow: 0px 1px 3px #666666;
  box-shadow: 0px 1px 3px #666666;
  text-shadow: 1px 1px 3px #666666;
  background: -webkit-gradient(linear, 0 0, 0 100%, from(#00e1ff), to(#0094e1));
  background: -moz-linear-gradient(top, #00e1ff, #0094e1);

`
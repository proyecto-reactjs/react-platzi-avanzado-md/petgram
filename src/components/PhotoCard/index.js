
import React from 'react'
import { Article, Img, ImgWrapper } from './styles'
import { FavButton } from '../FavButton'
import { useNearScreen } from '../../hooks/useNearScreen'
import { useMutationeToogleLike } from '../../hooks/useMutationeToogleLike'

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

const DEFAULT_IMAGE = 'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'

export const PhotoCard = ({ id = 0,liked, likes = 0, src = DEFAULT_IMAGE }) => {
  const [show, ref] = useNearScreen()
  const { mutation, mutationLoading, mutationError } = useMutationeToogleLike()
  const [open, setOpen] = React.useState(true);

  const handleFavClick = () => {
    mutation({
      variables: {
        input: { id }
      }
    }).catch( (err) => {console.log('Debe ingresar para realizar cambios')});
  }
  const errorLike = mutationError && 'Debe ingresar para realizar cambios'
    // console.log('{ mutation, mutationLoading, mutationError }', { mutation, mutationLoading, mutationError })
  return (
    <Article ref={ref}>
      {show &&
        <>
          <Link to={`/detail/${id}`}>
            <ImgWrapper>
              <Img src={src} />
            </ImgWrapper>
          </Link>

          <FavButton
            like={liked} likes={likes}
            onClick={handleFavClick} />
        </>
        
      }
      { errorLike &&
          <Stack sx={{ width: '100%', border: '1px solid red'}} spacing={2}>
                <Alert severity="error">{errorLike}</Alert>   
           </Stack>
      }
    </Article>
  )
}


PhotoCard.propTypes = {
  id: PropTypes.string.isRequired,
  liked :PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
  likes: function (props, propName, componentName) {
    const propValue = props[propName]

    if (propValue === undefined) {
      return new Error(`${componentName} - ${propName} value MUST be defined`)
    }

    if (propValue < 0) {
      return new Error(`${componentName} - ${propName} value MUST be positive`)
    }
  }
}
import styled from 'styled-components'
import { fadeIn } from '../../styles/animations'

export const List = styled.ul`
  animation: all .3s ease;
  display: flex;
  overflow: scroll;
  width: 100%;
  justify-content:normal;
  
  /* &::-webkit-scrollbar {
    display: none;
  } */

  &::-webkit-scrollbar {
  width: 10px;
  height: 10px;
  }
  &::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 3px rgba(1, 1, 1, 0.5);
          box-shadow: inset 0 0 3px rgba(1, 1, 1, 0.5);
  border-radius: 10px;
  }
  &::-webkit-scrollbar-thumb {
  border-radius: 10px;
  -webkit-box-shadow: inset 0 0 6px rgba(1, 1, 1, 0.5);
          box-shadow: inset 0 0 6px rgba(1, 1, 1, 0.5);
  }
  &.fixed {
    ${fadeIn({ time: '1.5s' })};
    background: #fff;
    border-radius: 60px;
    box-shadow: 0 0 20px rgba(0, 0, 0, .3);
    left: auto;
    margin-top: -20px;
    padding: 5px;
    position: fixed;
    top: 0;
    transform: scale(.5);
    z-index: 1;
    width: auto;
  }

  @media (max-width: 500px) {
  &.fixed{
    left: 0;
    width: -webkit-fill-available;
  }
  }

  
`

export const Item = styled.li`
  padding: 0 8px;
`
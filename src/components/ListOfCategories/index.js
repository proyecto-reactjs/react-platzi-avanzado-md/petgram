import React, { useEffect, useState } from 'react'
import { Category } from '../Category'
import { List, Item } from './styles'
import { useCategoriesData } from '../../hooks/useCategoriesData'
import Box from '@mui/material/Box'
import LinearProgress from '@mui/material/LinearProgress'

const ListOfCategoriesComponent = () => {
  const { loading, categories } = useCategoriesData()
  const [showFixed, setShowFixed] = useState(false)

  useEffect( ()=> {
    const onScroll = (e) => {
      const newShowFixed = window.scrollY > 200
      showFixed !== newShowFixed && setShowFixed(newShowFixed)
    }

    document.addEventListener('scroll', onScroll)

    return () => {
      document.removeEventListener('scroll', onScroll)
    }
  }, [showFixed])

  const renderList = (fixed) => {
    return (
      <>
        <List className={fixed ? 'fixed' : ''}>
          {
            loading
              ? 
                  <Box sx={{ width: '100%' }}>
                    <LinearProgress />
                  </Box>

              // <Item key='loading'><Category /></Item>
              : 
              categories.map(category =>
                <Item key={category.id}><Category {...category} path={`/pet/${category.id}`} /></Item>
              ) 
          }
        </List>   
      </>
    )
  }

  return (
    <>
      {renderList()}
      {showFixed && renderList(true)}
    </>
  )
}

export const ListOfCategories = React.memo(ListOfCategoriesComponent)

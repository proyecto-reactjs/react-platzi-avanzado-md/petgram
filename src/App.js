import React from 'react'
import { GlobalStyles } from './styles/GlobalStyles'
import { AppRouter } from './router/AppRouter'
import { AuthProvider } from './context/AppContext'

export default function () {
  return (
    <>
      <AuthProvider>
        <GlobalStyles />
        <AppRouter/>
      </AuthProvider>,
    </>
    
  )
}
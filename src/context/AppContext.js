import { createContext, useState, useContext } from 'react'
import { useLocalStorage } from '../hooks/useLocalStorage'

const AuthContext = createContext()


export function useAuthContext () {
    return useContext(AuthContext)
  }

  export function AuthProvider ({ children }) {
    const [token, setToken] = useLocalStorage('token', '')
    const [isAuth, setIsAuth] = useState(() => !!token)

    const toggleAuth = (token) => {

        if (!token || typeof token !== 'string') {
          setIsAuth(false)
          return
        }

        setToken(token)
        setIsAuth(true)
      }
    const removeAuth = () => {
      setIsAuth(false);
      window.localStorage.removeItem("token");
    }

    return (
      <AuthContext.Provider value={{ isAuth, token, toggleAuth, removeAuth }}>
        {children}
      </AuthContext.Provider>
    )
  }

  // La parte de guardar el token , no lo hice con ssesion Storage, sino con Local Storage, me guie de un compañero y su reuso del Local Storage
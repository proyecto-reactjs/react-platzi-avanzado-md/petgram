import { useQuery, gql } from '@apollo/client'
import React from 'react'
import {ListOfFavs} from '../components/ListOfFavs'
import LoadingIcons from 'react-loading-icons'

import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

const GET_FAVORITES = gql`
query getFavs {
    favs {
      id
      categoryId
      src
      likes
      userId
    }
  }
`;

const FavsWithQuery = () => {
    const { loading, data, error } = useQuery(GET_FAVORITES, {
        fetchPolicy: 'cache-and-network'
      })
      return { loading, data, error }
};

export const RenderPropFavs = () => {
    const {loading, data, error} = FavsWithQuery();
    if(loading) return <div style = {{display:'flex',justifyContent:'center', width:'100%', height:'100%', marginTop :'28vh'}}><LoadingIcons.Circles fill="#00e1ff" stroke="#0094e1" strokeOpacity ={.125}  fillOpacity ={.5}speed={.75} height= {80}/></div>
    if(error) return    <>  
    <Stack sx={{ width: '100%', border: '1px solid red'}} spacing={2}>
    <Alert severity="error">Error, vuelva a ingresar</Alert>
  </Stack>
  </>

    const {favs} = data
    return <ListOfFavs favs={favs} />

}
import React from 'react'
import { ListOfPhotoCardsComponent } from '../components/ListOfPhotoCards'
import { withPhotos } from '../hocs/withPhotos'

const ListOfPhotoCardsCom = withPhotos(ListOfPhotoCardsComponent)
export const ListOfPhotoCards = React.memo(ListOfPhotoCardsCom)
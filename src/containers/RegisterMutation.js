import { gql, useMutation } from '@apollo/client';

const REGISTER = gql`
  mutation signup($input: UserCredentials!) {
    signup(input: $input)
  }
`;

export const useRegisterMutation = () => {
    const [registerMutation, { data:dataRegister, loading:loadingRegister, error:errorRegister }] = useMutation(REGISTER);  
    return {registerMutation,dataRegister,loadingRegister,errorRegister};
};
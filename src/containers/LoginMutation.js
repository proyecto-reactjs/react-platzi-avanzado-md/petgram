import { gql, useMutation } from '@apollo/client';

const LOGIN = gql`
mutation login($input: UserCredentials!) {
  login(input: $input)
}
`
export const useLoginMutation = () => {
    const [loginMutation, { data:dataLogin, loading:loadingLogin, error:errorLogin }] = useMutation(LOGIN);  
    return {loginMutation,dataLogin,loadingLogin,errorLogin};
};

import React from 'react'
import { useQuery  } from '@apollo/client';
import { gql } from '@apollo/client';

import { PhotoCard } from '../components/PhotoCard'
import LoadingIcons from 'react-loading-icons'

const query = gql`
    query getSinglePhoto($id:ID!) {
        photo(id:$id) {
            id
            categoryId
            src
            likes
            userId
            liked
        }
    }
`

export const PhotoCardWithQuery = ({ id }) => {
  console.log(id)
  const { loading, error, data } = useQuery(query, {
    variables: {
      id: id
    }
  })
  if (error) {
    return <h2>Internal Server Error</h2>
  }
  if (loading) {
    return <div style = {{display:'flex',justifyContent:'center', width:'100%', height:'100%', marginTop :'28vh'}}><LoadingIcons.Circles fill="#00e1ff" stroke="#0094e1" strokeOpacity ={.125}  fillOpacity ={.5}speed={.75} height= {80}/></div>
  }

  return (
    <PhotoCard {...data.photo} />
  )
}

// const renderProp = ({ loading, error, data }) => {
//   if (loading) return <p>Loading...</p>
//   if (error) return <p>Error! ⛔</p>
//   return <PhotoCard {...data.photo} />
// }

// export const PhotoCardWithQuery = ({ id }) => (
//   <useQuery query={query} variables={{ id }}>
//     {renderProp}
//   </useQuery>
// )
import React,{Suspense} from'react'
import { BrowserRouter as Router, Switch, Route,Redirect } from 'react-router-dom'
import { PrivateRoute } from './PrivateRoute'
import { NavBar } from '../components/NavBar'
import { useAuthContext } from '../context/AppContext'
import  Home  from '../pages/Home'
// import { Detail } from '../pages/Detail'
// import { RegisterPage } from'../pages/RegisterPage'
import { NotFound } from '../pages/NotFound'
import {Logo} from '../components/Logo'

const RegisterPage = React.lazy(() => import('../pages/RegisterPage').then(module => ({ default: module.RegisterPage }))) //Realizo de esta forma el React.lazy, apra no modifficar en mis pages
const LoginPage = React.lazy(() => import('../pages/LoginPage').then(module => ({ default: module.LoginPage }))) //Realizo de esta forma el React.lazy, apra no modifficar en mis pages
const Detail = React.lazy(() => import('../pages/Detail').then(module => ({ default: module.Detail })))
// const Home = React.lazy(() => import('../pages/Home'))


export const AppRouter = () =>{
    const { isAuth } = useAuthContext()
    return(
        <Router>
            <div style={{ display:'flex',justifyContent:'center' }}>
                <Logo />
            </div>
            <Suspense fallback = {<div/>}>
                <Switch>
                
                    <Route exact path='/' render={(props) =><Home {...props} />} />
                    <Route exact path='/pet/:id' render={(props) =><Home {...props} />} />
                    <Route exact path='/detail/:detailId' component={Detail} />
                    <Route exact path='/404' component={NotFound} />
                    {!isAuth && <Route exact component={RegisterPage} path="/register" />}
                    {!isAuth && <Route exact component={LoginPage} path="/login" />}
                    {!isAuth && <Redirect from="/favs" to="/register" />}
                    {!isAuth && <Redirect from="/user" to="/register" />}
                    {isAuth && <Redirect from="/register" to="/" />}
                    {isAuth && <Redirect from="/login" to="/" />}
                    <PrivateRoute isAuth={isAuth} />
                
                    {/* <Route path="*" component={ NotFound } /> */}
                
                </Switch>
            </Suspense>
            <NavBar />
        </Router>
    )
}

import React,{Suspense} from'react'
import { Route,Redirect,Switch } from 'react-router-dom'
// import { Favs } from'../pages/Favs'
// import { Profile } from'../pages/Profile'
import { NotFound } from '../pages/NotFound'

const Favs = React.lazy(() => import('../pages/Favs').then(module => ({ default: module.Favs })))
const Profile = React.lazy(() => import('../pages/Profile').then(module => ({ default: module.Profile })))

export const PrivateRoute = ({isAuth}) => {
  return (
    <Suspense fallback = {<div/>}>
      {
        isAuth ? (
          <>

            <Switch>
              <Route exact path='/favs' component={Favs} />
              <Route exact path='/user' component={Profile} />
              <Redirect to="/404" component={ NotFound } />
            </Switch>

          </>
        ) : (
          <>
            <Redirect to="/404" component={ NotFound } />

          </>
        )
        
      }

    </Suspense>
  )
}
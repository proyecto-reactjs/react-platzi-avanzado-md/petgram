const HtmlWebpackPlugin = require('html-webpack-plugin') // Estamos trayendo el módulo que se acababa de intalar desde npm, y almacenándolo en una constante.
const WebpackPwaManifestPlugin = require('webpack-pwa-manifest')
const WorkboxWebpackPlugin = require('workbox-webpack-plugin')

const path = require('path')// el modulo path sirve para trabajar con archivos y rutas de directorios,Require es la sentencia que se usa para importar un módulo y 'path' es el nombre del módulo que deseamos importar. Dicho módulo se almacena en una constante llamada "path".

module.exports = { // Con "module.exports" estamos exportando hacia afuera código Javascript. Todo el código que esté dentro de exports se podrá usar fuera del archivo / módulo que estamos programando.
  entry: { // Punto de entrada
    main: path.resolve(__dirname, './src/index.js') // Entry main: sirve para indicar el archivo de Javascript de entrada de este proyecto.El ./src/index.js ya es el que viene por defecto, epro igual lo coloco como guia.

  },
  output: { // Punto de salida
    path: path.resolve(__dirname, './dist'), // por defecto crea en dist, puedo modificar si quiero otro nombre de carpeta
    filename: 'app.bundle.js', // Output filename: Sirve para decirle el nombre del archivo del bundle que va a generar.
    publicPath: '/' // este es el path donde encontrara el bundle, lo añadimos para evitar el el error: ERRO_ABORTED 404

  },
  plugins: [
    new HtmlWebpackPlugin(// usamos la instancia de este plugin
      { template: './src/index.html' }
    ),
    new WebpackPwaManifestPlugin(
      {
        name: 'Petgram -Tu app de fotos de mascotas',
        short_name: 'Petgram 🐶',
        description: 'Con Petgram puedes encontrar fotos de animales domesticos',
        background_color: '#fff',
        theme_color:'#b1a',
        icons: [
          {
            src: path.resolve('./src/assets/icon.png'),
            sizes: [96, 128,144,192, 256, 384, 512], //tamaños del icono, comunes en dispositivos moviles para pwa
            purpose: 'any maskable' // <-- Añade esta línea para solucionar el error : "A maskable icon ensures that the image fills the entire shape without being letterboxed when installing the app on a device.", y con el "any" soluciona un error presente en el installable
          }
        ]
      }
    ),
    new WorkboxWebpackPlugin.GenerateSW({
      maximumFileSizeToCacheInBytes: 5000000, // para evitar el warning de: WARNING in /app.bundle.js is 2.71 MB, and won't be precached. Configure maximumFileSizeToCacheInBytes to change this limit.
      runtimeCaching: [
        {
          urlPattern: new RegExp('https://(res.cloudinary.com|images.unsplash.com)'),
          handler: 'CacheFirst',
          options: {
            cacheName: 'images'
          },
        },
        {
          urlPattern: new RegExp('https://petgram-serve-jorge-vicuna.vercel.app'),
          handler: 'NetworkFirst',
          options: {
            cacheName: 'api'
          }
        }
      ]
    })
    
  ],
  devServer: {
    historyApiFallback: true, //Evitamos este error cuando recarguemos en una vista que no sea '/', salga el error GET http://localhost:8081/detail/0 404 (Not Found)
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env'],
            [
              '@babel/preset-react',
              {
                runtime: 'automatic' // por defecto es classic, con esto no seria necesario usar: import React from "react";
              }
            ]
          ],
          plugins: ["@emotion"]
        }

      }
    ]
  }
  // resolve: {
  //   alias: {
  //     'react-dom$': 'react-dom/profiling',
  //     'scheduler/tracing': 'scheduler/tracing-profiling',
  //   },
  // },
}
